// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#include "config.h"

#include <cmath>
#include <cstddef>
#include <exception>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include <dune/common/fvector.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/grid/io/file/vtk/function.hh>
#include <dune/grid/yaspgrid.hh>

#include <dune/localfunctions/lagrange/qk/qklocalbasis.hh>

// parameters

const Dune::FieldVector<double,1> gravity = {-1.0};

const double alpha = -2.3;
const double k0 = 2.2e-05;
const double n = 4.17;
const double tau = -1.1;
const double theta_r = 0.03;
const double theta_s = 0.32;
const double m = 1.0 - 1.0 / n;

// paramterization functions

double saturation (const double u)
{
  if (u >= 0.0)
    return 1.0;
  return std::pow(1 + std::pow(alpha * u, n), -m);
}

double conductivity (const double saturation)
{
  return std::pow(saturation,tau)
    * std::pow(1 - std::pow(1 - std::pow(saturation,1.0/m) , m) , 2);
}

const double final[] = {
  /*  0*/ 6.6545259754572319e-10,
  /*  3*/ -8.6363616626608276e-03,
  /*  6*/ -1.7272681447116564e-02,
  /*  9*/ -1.7272679576401422e-02,
  /* 12*/ -2.5908839526122922e-02,
  /* 15*/ -3.4544626096371843e-02,
  /* 18*/ -3.4544622038064028e-02,
  /* 21*/ -4.3179726815794718e-02,
  /* 24*/ -5.1813740181635433e-02,
  /* 27*/ -5.1813733859772310e-02,
  /* 30*/ -6.0446147770478111e-02,
  /* 33*/ -6.9076351281382772e-02,
  /* 36*/ -6.9076342662875032e-02,
  /* 39*/ -7.7703624754920861e-02,
  /* 42*/ -8.6327170837782186e-02,
  /* 45*/ -8.6327159889897837e-02,
  /* 48*/ -9.4946053265243580e-02,
  /* 51*/ -1.0355927267714840e-01,
  /* 54*/ -1.0355925934370766e-01,
  /* 57*/ -1.1216567793419262e-01,
  /* 60*/ -1.2076406233357639e-01,
  /* 63*/ -1.2076404652251709e-01,
  /* 66*/ -1.2935305217683721e-01,
  /* 69*/ -1.3793122482021106e-01,
  /* 72*/ -1.3793120639805820e-01,
  /* 75*/ -1.4649697256222219e-01,
  /* 78*/ -1.5504864462032350e-01,
  /* 81*/ -1.5504862341217923e-01,
  /* 84*/ -1.6358438395612845e-01,
  /* 87*/ -1.7210229597585924e-01,
  /* 90*/ -1.7210227177064136e-01,
  /* 93*/ -1.8060025542423136e-01,
  /* 96*/ -1.8907610532825050e-01,
  /* 99*/ -1.8907607788946593e-01,
  /*102*/ -1.9752743062900080e-01,
  /*105*/ -2.0595179157571583e-01,
  /*108*/ -2.0595176065831983e-01,
  /*111*/ -2.1434646039860589e-01,
  /*114*/ -2.2270869393857190e-01,
  /*117*/ -2.2270865931245448e-01,
  /*120*/ -2.3103542947808361e-01,
  /*123*/ -2.3932360175891035e-01,
  /*126*/ -2.3932356323921899e-01,
  /*129*/ -2.4756979418027880e-01,
  /*132*/ -2.5577060538202689e-01,
  /*135*/ -2.5577056286584854e-01,
  /*138*/ -2.6392225249688067e-01,
  /*141*/ -2.7202099200876040e-01,
  /*144*/ -2.7202094551663947e-01,
  /*147*/ -2.8006267271303387e-01,
  /*150*/ -2.8804321444621783e-01,
  /*153*/ -2.8804316416567072e-01,
  /*156*/ -2.9595810994705479e-01,
  /*159*/ -3.0380296316736655e-01,
  /*162*/ -3.0380290949414118e-01,
  /*165*/ -3.1157294137547531e-01,
  /*168*/ -3.1926337203316507e-01,
  /*171*/ -3.1926331560476162e-01,
  /*174*/ -3.2686914920976512e-01,
  /*177*/ -3.3438538448090022e-01,
  /*180*/ -3.3438532619614342e-01,
  /*183*/ -3.4180677490639777e-01,
  /*186*/ -3.4912829923512173e-01,
  /*189*/ -3.4912824025369771e-01,
  /*192*/ -3.5634455812711613e-01,
  /*195*/ -3.6345050243761989e-01,
  /*198*/ -3.6345044415470834e-01,
  /*201*/ -3.7044075973315232e-01,
  /*204*/ -3.7731037700461112e-01,
  /*207*/ -3.7731032099816358e-01,
  /*210*/ -3.8405415048389907e-01,
  /*213*/ -3.9066736131065954e-01,
  /*216*/ -3.9066730926233073e-01,
  /*219*/ -3.9714512783923889e-01,
  /*222*/ -4.0348311007883325e-01,
  /*225*/ -4.0348306367363307e-01,
  /*228*/ -4.0967690477734009e-01,
  /*231*/ -4.1572269332149059e-01,
  /*234*/ -4.1572265413516951e-01,
  /*237*/ -4.2161669968260973e-01,
  /*240*/ -4.2735575719689683e-01,
  /*243*/ -4.2735572658306514e-01,
  /*246*/ -4.3293684788520725e-01,
  /*249*/ -4.3835756620841065e-01,
  /*252*/ -4.3835754519862119e-01,
  /*255*/ -4.4361575541019999e-01,
  /*258*/ -4.4870985078277431e-01,
  /*261*/ -4.4870984001179609e-01,
  /*264*/ -4.5363862475397038e-01,
  /*267*/ -4.5840139799526441e-01,
  /*270*/ -4.5840139766056676e-01,
  /*273*/ -4.6299790031992133e-01,
  /*276*/ -4.6742834453716270e-01,
  /*279*/ -4.6742835439678238e-01,
  /*282*/ -4.7169340524871989e-01,
  /*285*/ -4.7579415703996636e-01,
  /*288*/ -4.7579417644745281e-01,
  /*291*/ -4.7973216837127880e-01,
  /*294*/ -4.8350931186495122e-01,
  /*297*/ -4.8350933983509919e-01,
  /*300*/ -4.8712796605866920e-01,
  /*303*/ -4.9059071068045979e-01,
  /*306*/ -4.9059074597601310e-01,
  /*309*/ -4.9390062535888185e-01,
  /*312*/ -4.9706088654131420e-01,
  /*315*/ -4.9706092776927630e-01,
  /*318*/ -5.0007514953036281e-01,
  /*321*/ -5.0294706595914362e-01,
  /*324*/ -5.0294711166578188e-01,
  /*327*/ -5.0568073382624867e-01,
  /*330*/ -5.0828015524647274e-01,
  /*333*/ -5.0828020400211749e-01,
  /*336*/ -5.1074973846476512e-01,
  /*339*/ -5.1309371514538860e-01,
  /*342*/ -5.1309376561304942e-01,
  /*345*/ -5.1531667855234087e-01,
  /*348*/ -5.1742297820377525e-01,
  /*351*/ -5.1742302918870564e-01,
  /*354*/ -5.1941727934887538e-01,
  /*357*/ -5.2130395070816993e-01,
  /*360*/ -5.2130400118826237e-01,
  /*363*/ -5.2308763186206053e-01,
  /*366*/ -5.2477262731846475e-01,
  /*369*/ -5.2477267645740289e-01,
  /*372*/ -5.2636347025967378e-01,
  /*375*/ -5.2786433358016771e-01,
  /*378*/ -5.2786438072649888e-01,
  /*381*/ -5.2927958043593104e-01,
  /*384*/ -5.3061320037267146e-01,
  /*387*/ -5.3061324504846108e-01,
  /*390*/ -5.3186933913412493e-01,
  /*393*/ -5.3305176568882706e-01,
  /*396*/ -5.3305180757147341e-01,
  /*399*/ -5.3416437566581865e-01,
  /*402*/ -5.3521069306610114e-01,
  /*405*/ -5.3521073196644420e-01,
  /*408*/ -5.3619434342625361e-01,
  /*411*/ -5.3711859230346715e-01,
  /*414*/ -5.3711862814268851e-01,
  /*417*/ -5.3798678577352954e-01,
  /*420*/ -5.3880192640740909e-01,
  /*423*/ -5.3880195919455887e-01,
  /*426*/ -5.3956707997053066e-01,
  /*429*/ -5.4028498854539031e-01,
  /*432*/ -5.4028501835498033e-01,
  /*435*/ -5.4095844333385545e-01,
  /*438*/ -5.4158993357311180e-01,
  /*441*/ -5.4158996073528598e-01,
  /*444*/ -5.4218198395903316e-01,
  /*447*/ -5.4273686319902603e-01,
};

template<class GridView>
class FluxVTKFunction :
  public Dune::VTKFunction<GridView>
{
  using Entity = typename GridView::template Codim<0>::Entity;
  using ctype = typename GridView::ctype;
  static constexpr std::size_t dim = 1;

  GridView gv_;
  const double *data_;

public:
  FluxVTKFunction(const GridView &gv, const double *data) :
    gv_(gv), data_(data)
  {}

  int ncomps() const override { return dim; }
  double evaluate(int comp, const Entity &e,
                  const Dune::FieldVector< ctype, dim > &xi) const override
  {
    using LB = Dune::QkLocalBasis<ctype, double, 2, dim>;
    static const LB lb{};

    auto celldata = data_ + lb.size() * gv_.indexSet().index(e);

    std::vector<typename LB::Traits::JacobianType> shapeJacobians;
    lb.evaluateJacobian(xi, shapeJacobians);

    auto JInvT = e.geometry().jacobianInverseTransposed(xi);
    Dune::FieldVector<ctype, dim> flux; flux = 0;
    for(unsigned i = 0; i < lb.size(); ++i)
    {
      Dune::FieldVector<ctype, dim> gradphi;
      JInvT.mv(shapeJacobians[i][0], gradphi);
      flux.axpy(celldata[i], gradphi);
    }

    // read solution value
    std::vector<typename LB::Traits::RangeType> yb;
    lb.evaluateFunction(xi,yb);
    typename LB::Traits::RangeType u;
    for(unsigned i = 0; i < lb.size(); ++i){
      u.axpy(celldata[i],yb[i]);
    }

    // apply parameterization functions
    flux -= gravity;
    flux *= - k0 * conductivity(saturation(u));

    return flux[comp];
  }
  std::string name() const override { return "flux"; }
};

int main(int argc, char** argv)
{
  try{
    // Maybe initialize MPI
    Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);

    Dune::YaspGrid<1> grid({1.0}, {50});
    auto gv = grid.leafGridView();

    FluxVTKFunction<decltype(gv)> fluxfunc(gv, final);

    std::ofstream cgnuplot("final.dat");
    cgnuplot << std::setprecision(20);
    for(const auto &e : elements(gv))
    {
      for(Dune::FieldVector<double, 1> xl : {0.125, 0.375, 0.625, 0.825})
      {
        cgnuplot << e.geometry().global(xl) << "\t"
                 << fluxfunc.evaluate(0, e, xl) << "\n";
      }
    }
    return 0;
  }
  catch (std::exception &e){
    std::cerr << "std reported error: " << e.what() << std::endl;
  }
  catch (...){
    std::cerr << "Unknown exception thrown!" << std::endl;
  }
}
