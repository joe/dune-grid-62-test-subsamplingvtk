#!/usr/bin/env gnuplot

set term svg
set output "final.svg"
set ytics format "%e"
plot "final.dat" with lines
